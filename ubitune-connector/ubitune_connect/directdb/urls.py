from django.conf.urls import url
from django.urls import path

from . import views

urlpatterns = [
        path('refresh_token/', views.refresh_token),
        path('input/', views.input, name = 'input'),
        path('authenticate/', views.authenticate_user, name = 'auth'),
        path('get_user_data/', views.get_user_data, name = 'user_data'),
        path('update_user_locations/', views.update_user_locations),
        path('create_hub/', views.create_hub),
        path('delete_hub/', views.delete_hub),
        path('get_hub/', views.get_hub),
        url(r'^$', views.index, name = 'index'),
        ]
