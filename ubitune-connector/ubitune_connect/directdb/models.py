# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.gis.db import models
from django.contrib.postgres.fields import ArrayField
from datetime import timedelta

class DataPoint(models.Model):
    json_data = models.TextField()

    def __str__(self):
        return self.json_data

class UserData(models.Model):
    name = models.TextField(blank = True)
    access_token = models.TextField()
    refresh_token = models.TextField()
    expires_in = models.DurationField()
    fav_songs = ArrayField(models.TextField(blank = True), size = 5)
    spotify_id = models.TextField(unique = True)
    state = models.TextField(blank = True)

    def __str__(self):
        data = {'name': self.name,
                'access_token': self.access_token,
                'expires_in': self.expires_in.total_seconds(),
                'fav_songs': self.fav_songs,
                'spotify_id': self.spotify_id,
                'state': self.state}
        return str(data)

    def return_dict(self):
        data = {'name': self.name,
                'access_token': self.access_token,
                'expires_in': self.expires_in.total_seconds(),
                'fav_songs': self.fav_songs,
                'spotify_id': self.spotify_id,
                'state': self.state}
        return data

class Hub(models.Model):
    user_list = ArrayField(models.TextField())
    distance_range = models.IntegerField()
    hub_id = models.TextField(unique = True)
    hub_location = models.PointField(srid = 4326)
    def __str__(self):
        return str(self.hub_id)

class UserLocation(models.Model):
    user_location = models.PointField(srid = 4326)
    spotify_id = models.TextField(unique = True)
    hub_list = ArrayField(models.IntegerField())
    def __str__(self):
        return self.spotify_id + str(self.user_location)
