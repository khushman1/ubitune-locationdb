# -2Y*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render
from django.urls import reverse
from django.contrib.gis.geos import GEOSGeometry
from datetime import timedelta
import json
from django.db.models import F
from django.db.models.expressions import CombinedExpression, Value, RawSQL, Func
from django.contrib.gis.measure import D
from django.contrib.gis.db.models.functions import Distance

from django.http import HttpResponse, HttpResponseForbidden, HttpResponseNotFound, JsonResponse
from .models import DataPoint, UserData, Hub, UserLocation

from . import spotify_util as su

@csrf_exempt
def index(request):
    return HttpResponse("Hola")

@csrf_exempt
def input(request):
    json = request.POST
    if json != '':
        d = DataPoint(json_data=json)
        d.save()
    return HttpResponse("Yum")

@csrf_exempt
def authenticate_user(request):
    error = request.GET.get('error', '')
    if error != '':
        return HttpResponseForbidden('Giff me access!')
    code = request.GET.get('code', '')
    if code == '':
        return HttpResponseForbidden()
    state = request.GET.get('state', '')
    resp = su.get_access_token(code)
    if resp == -1:
        #print('Access token error')
        return HttpResponseNotFound("Access Token not generated")
    access_token = resp.get('access_token')
    expires_in = timedelta(seconds = resp.get('expires_in'))
    refresh_token = resp.get('refresh_token')
    resp = su.get_user_data(access_token)
    if resp == -1:
        #print('User data error')
        return HttpResponseNotFound("Coudln't get user data")
    spotify_id = resp.get('id')
    try:
        u = UserData.objects.get(spotify_id = spotify_id)
    except UserData.DoesNotExist:
        name = resp.get('display_name')
        fav_songs = su.get_user_songs(access_token)
        #print(fav_songs)
        if fav_songs == -1:
            #print('top track error')
            return HttpResponseNotFound("Couldn't get top tracks")
        u = UserData(name = name, spotify_id = spotify_id, access_token = access_token,
                refresh_token = refresh_token, expires_in = expires_in,
                fav_songs = fav_songs, state = state)
    u.access_token = access_token
    u.refresh_token = refresh_token
    u.expires_in = expires_in
    u.state = state
    u.save()
    return HttpResponse('Welcome')

@csrf_exempt
def refresh_token(request):
    spotify_id = request.GET.get('spotify_id')
    try:
        user = UserData.objects.get(spotify_id = spotify_id)
    except UserData.DoesNotExist:
        return HttpResponseForbidden()
    tokens = su.get_refresh_token(user)
    access_token = tokens.get('access_token', '')
    if access_token != '':
        user.access_token = access_token
        user.expires_in = timedelta(seconds = tokens.get('expires_in'))
        user.save()
    data = {'access_token': user.access_token,
            'expires_in': user.expires_in.total_seconds()}
    return JsonResponse(data)

@csrf_exempt
def get_user_data(request):
    state = str(request.GET.get('state', ''))
    spotify_id = request.GET.get('spotify_id', '')
    #print(pretty_request(request))
    if state == '' and spotify_id == '':
        return HttpResponseForbidden()
    try:
        if spotify_id != '':
            #print("this is wrong")
            #print(spotify_id)
            user = UserData.objects.get(spotify_id = spotify_id)
        elif state != '':
            #print(state)
            #print("this is right")
            user = UserData.objects.get(state = state)
    except UserData.DoesNotExist:
        return HttpResponseNotFound()
    try:
        user
    except NameError:
        return HttpResponseForbidden()
    return JsonResponse(user.return_dict())

def pretty_request(request):
    headers = ''
    for header, value in request.META.items():
        if not header.startswith('HTTP'):
            continue
        header = '-'.join([h.capitalize() for h in header[5:].lower().split('_')])
        headers += '{}: {}\n'.format(header, value)

    return (
        '{method} HTTP/1.1\n'
        'Content-Length: {content_length}\n'
        'Content-Type: {content_type}\n'
        '{headers}\n\n'
        '{body}'
    ).format(
        method=request.method,
        content_length=request.META['CONTENT_LENGTH'],
        content_type=request.META['CONTENT_TYPE'],
        headers=headers,
        body=request.body,
    )

def get_request_body(request):    
    return json.loads(request)

def get_spotify_id_from_post_request(request):
    request_body = get_request_body(request.body.decode('utf-8'))
    return request_body, request_body.get('spotify_id')

def get_latlong_GEOS(input_location):
    latitude = input_location.get('latitude')
    longitude = input_location.get('longitude')
    try:
        latlong_string = 'POINT(' + str(longitude) + ' ' + str(latitude) + ')'
    except KeyError:
        return -1
    latlong_geos = GEOSGeometry(latlong_string, srid = 4326)
    return latlong_geos

@csrf_exempt
def create_hub(request):
    print(pretty_request(request))
    request, hub_id = get_spotify_id_from_post_request(request)
    distance_range = request.get('hubRange', '')
    hub_location = get_latlong_GEOS(request.get('hubLocation'))
    if hub_id == '' or hub_location == -1:
        return HttpResponseNotFound()
    if distance_range == '' or hub_location == '':
        return HttpResponseForbidden()
    try:
        hub = Hub.objects.get(hub_id = hub_id)
        UserLocation.objects.filter(spotify_id__in = hub.user_list).update(hub_list = ArrayRemove('hub_list', hub.id))
    except Hub.DoesNotExist:
        hub = Hub(hub_id = hub_id, distance_range = distance_range,
                hub_location = hub_location, user_list = [])
    hub.hub_location = hub_location
    hub.distance_range = distance_range
    user_list = UserLocation.objects.filter(user_location__distance_lte
            = (hub_location, D(m = distance_range)))
    hub.user_list = list(user_list.values_list('spotify_id', flat = True))
    hub.save()
    user_list.update(hub_list = CombinedExpression(F('hub_list'), '||', Value([hub.id])))
    return HttpResponse("Created")

@csrf_exempt
def delete_hub(request):
    request, spotify_id = get_spotify_id_from_post_request(request)
    if spotify_id == '':
        return HttpResponseForbidden()
    try:
        hub = Hub.objects.get(hub_id = spotify_id)
        # Find all the users in the hub and remove the hub from all their lists
        UserLocation.objects.filter(spotify_id__in = hub.user_list).update(hub_list = ArrayRemove('hub_list', hub.id))
        hub.delete()
    except Hub.DoesNotExist:
        return HttpResponseForbidden()
    return HttpResponse()

@csrf_exempt
def get_hub(request):
    spotify_id = request.GET.get('spotify_id', '')
    if spotify_id == '':
        return HttpResponseForbidden()
    try:
        user = UserLocation.objects.get(spotify_id = spotify_id)
    except Hub.DoesNotExist:
        return HttpResponseNotFound()
    sorted_hub_list = list(Hub.objects.filter(id__in = user.hub_list).annotate(distance = Distance(F('hub_location'), user.user_location)).order_by('distance').values_list('hub_id', flat = True))
    return JsonResponse({'user_list': sorted_hub_list})

@csrf_exempt
def update_user_locations(request):
    request, spotify_id = get_spotify_id_from_post_request(request)
    if spotify_id == '':
        return HttpResponseForbidden()
    user_latlong = get_latlong_GEOS(request.get('userLocation'))
    if user_latlong == -1:
        return HttpResponseNotFound()
    try:
        user = UserLocation.objects.get(spotify_id = spotify_id)
    except UserLocation.DoesNotExist:
        user = UserLocation(user_location = user_latlong, spotify_id = spotify_id, hub_list = [])
    hubs = user.hub_list
    # Delete all user entries from hubs
    Hub.objects.filter(id__in = hubs).update(user_list = ArrayRemove('user_list', spotify_id))
    # Update all new hubs with those users
    hubs_attached_to = Hub.objects.filter(hub_location__distance_lte = (user_latlong, F('distance_range')))
    hubs_attached_to.update(user_list = CombinedExpression(F('user_list'), '||', Value([spotify_id])))
    user.hub_list = list(hubs_attached_to.values_list('id', flat = True))
    user.save()
    return HttpResponse()


class ArrayRemove(Func):

    function = 'array_remove'
    template = "%(function)s(%(expressions)s, %(element)s)"
    arity = 1

    def __init__(self, expression: str, element, **extra):
        if not isinstance(element, (str, int)):
            raise TypeError(
                'Type of "{element}" must be int or str, '.format(element = element) + 
                'not "{elemtype}".'.format(elemtype = type(element).__name__)
            )

        super().__init__(
            expression,
            element=isinstance(element, int) and element or "'{element}'".format(element = element),
            **extra,
        )
