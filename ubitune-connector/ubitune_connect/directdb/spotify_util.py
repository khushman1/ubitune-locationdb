# -2Y*- coding: utf-8 -*-   
from __future__ import unicode_literals

from django.conf import settings
import requests as req
import json
from datetime import timedelta

def get_access_token(code):
    spotify_token_url = 'https://accounts.spotify.com/api/token'
    auth_data = {"grant_type": "authorization_code",
            "code": code,
            "redirect_uri": settings.SPOTIFY_REDIRECT_URI,
            "client_id": settings.SPOTIFY_CLIENT_ID,
            "client_secret": settings.SPOTIFY_CLIENT_SECRET}
    r = req.post(spotify_token_url, data = auth_data)
    if r.status_code != 200:
        return -1
    return json.loads(r.text)

def get_user_data(access_token):
    spotify_user_url = "https://api.spotify.com/v1/me"
    headers = {"Authorization": "Bearer " + access_token}
    r = req.get(spotify_user_url, headers = headers)
    if r.status_code != 200:
        return -1
    return json.loads(r.text)

def get_user_songs(access_token):
    spotify_top_tracks_url = "https://api.spotify.com/v1/me/top/tracks"
    headers = {"Authorization": "Bearer " + access_token}
    query_data = {"limit": 5}
    r = req.get(spotify_top_tracks_url, headers = headers, params = query_data)
    if r.status_code != 200:
        return -1
    resp = json.loads(r.text).get("items")
    fav_songs = [item.get("id") for item in resp]
    return fav_songs

def get_refresh_token(user):
    spotify_token_url = 'https://accounts.spotify.com/api/token'
    auth_data = {"grant_type": "refresh_token",
            "refresh_token": user.refresh_token,
            "client_id": settings.SPOTIFY_CLIENT_ID,
            "client_secret": settings.SPOTIFY_CLIENT_SECRET}
    r = req.post(spotify_token_url, data = auth_data)
    print(r.text)
    if r.status_code != 200:
        return -1
    return json.loads(r.text)
