from django.test import TestCase, Client
from directdb.models import UserData, UserLocation, Hub
from django.contrib.gis.geos import GEOSGeometry 

class HubTestCase(TestCase):
    def setUp(self):
        self.client = Client()
        self.u1 = UserLocation.objects.create(spotify_id = "kames1", user_location = GEOSGeometry('POINT(-84.3958206 33.7743312)', srid = 4326), hub_list = [])
        self.u2 = UserLocation.objects.create(spotify_id = "kames2", user_location = GEOSGeometry('POINT(-84.4008824 33.7748511)', srid = 4326), hub_list = [])

    def test_create_delete_hub(self):
        self.create_hub_test()
        self.u1.refresh_from_db()
        self.assertEqual(len(self.u1.hub_list), 1)
        self.assertEqual(len(self.u2.hub_list), 0)
        self.update_u2_location()
        self.u2.refresh_from_db()
        self.assertEqual(len(self.u2.hub_list), 1)
        self.assertEqual(Hub.objects.get(hub_id = 'kames').user_list, ['kames1', 'kames2'])
        self.delete_hub_test()
        self.u1.refresh_from_db()
        self.u2.refresh_from_db()
        self.assertEqual(len(self.u1.hub_list), 0)
        self.assertEqual(len(self.u2.hub_list), 0)

    def create_hub_test(self):
        response = self.client.post('/feedme/create_hub/', {'spotify_id':'kames', 'range': '5', 'latitude': '33.7743312', 'longitude': '-84.3958206'})
        self.assertEqual(response.status_code, 200)
        

    def delete_hub_test(self):
        response = self.client.post('/feedme/delete_hub/', {'spotify_id':'kames'})
        self.assertEqual(response.status_code, 200)

    def update_u2_location(self):
        response = self.client.post('/feedme/update_user_locations/', {'spotify_id':'kames2', 'latitude': '33.7743412', 'longitude': '-84.3958210'})
        self.assertEqual(response.status_code, 200)
