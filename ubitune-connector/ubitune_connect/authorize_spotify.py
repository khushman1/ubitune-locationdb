import requests

data = {"client_id": "ee2ce2ba0ae3429398767f1426bcac80",
        "response_type": "code",
        "redirect_uri": "http://35.231.156.227/feedme/authenticate/",
        "state": "1234567890",
        "scope": "user-top-read playlist-modify-private playlist-read-private user-read-private"}
r = requests.get('https://accounts.spotify.com/authorize/', params=data)
print(r.url)
