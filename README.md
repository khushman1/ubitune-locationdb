Activate the virtual environment after cloning in home directory by

`source ./ubituneenv/bin/activate`

And run the server in this environment by

`authbind --deep python3 manage.py runserver 0.0.0.0:80`
